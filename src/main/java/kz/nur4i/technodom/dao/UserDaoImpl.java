package kz.nur4i.technodom.dao;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import kz.nur4i.technodom.model.User;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Aibek
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

    @Override
    public List<User> list() {
        Query query = getSession().createQuery("from User");
        return query.list();
    }

    @Override
    public User create(User user) {
        user.setStatus("offline");
        save(user);
        return user;
    }

    @Override
    public User update(User user) {
        getSession().update(user);
        return user;
    }

    @Override
    public User get(Long id) {
        return getSession().get(User.class, id);
    }

    @Override
    public List<User> getByStatus(String status) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> userRoot = criteria.from(User.class);
        criteria.select(userRoot);
        if (status != null && !status.isEmpty()) {
            criteria.where(builder.equal(userRoot.get("status"), status));
        }
        List<User> list = getSession().createQuery(criteria).list();
        return list;
    }

}
