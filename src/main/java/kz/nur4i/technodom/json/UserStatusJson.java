package kz.nur4i.technodom.json;

/**
 *
 * @author Aibek
 */
public class UserStatusJson {

    private Long userId;
    private String oldStatus;
    private String newStatus;

    public UserStatusJson(Long userId, String oldStatus, String newStatus) {
        this.userId = userId;
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

}
