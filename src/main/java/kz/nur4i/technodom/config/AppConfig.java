package kz.nur4i.technodom.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Aibek
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "kz.nur4i.technodom")
public class AppConfig {

}
