package kz.nur4i.technodom.json;

import java.util.List;
import kz.nur4i.technodom.model.User;

/**
 *
 * @author Aibek
 */
public class StatsJson {

    private List<User> users;
    private Long timestamp;

    public StatsJson(List<User> users, Long timestamp) {
        this.users = users;
        this.timestamp = timestamp;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
