package kz.nur4i.technodom.json;

/**
 *
 * @author Aibek
 */
public class StatsReqJson {

    private String status;
    private Long timestamp;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
