package kz.nur4i.technodom.service;

import java.util.List;
import kz.nur4i.technodom.model.User;

/**
 *
 * @author Aibek
 */
public interface UserService {

    User createUser(User user);

    User getUser(Long id);

    String changeStatus(Long id, String newStatus);

    List<User> getByStatus(String status);
}
