package kz.nur4i.technodom.dao;

import java.util.List;
import kz.nur4i.technodom.model.User;

/**
 *
 * @author Aibek
 */
public interface UserDao {

    List<User> list();

    User create(User user);

    User update(User user);

    User get(Long id);

    List<User> getByStatus(String status);
}
