package kz.nur4i.technodom.controller;

import java.util.List;
import kz.nur4i.technodom.json.StatsJson;
import kz.nur4i.technodom.json.StatsReqJson;
import kz.nur4i.technodom.json.UserIdJson;
import kz.nur4i.technodom.json.UserStatusJson;
import kz.nur4i.technodom.model.User;
import kz.nur4i.technodom.service.UserService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Aibek
 */
@RestController
public class WsController {

    @Autowired
    private UserService userService;

    /**
     * Возможные коды ответов:<br/>
     * 201 - Created<br/>
     * 204 - No Content <br/>
     * 206 - Partial Content <br/>
     * 409 - Conflict<br/>
     *
     * @param user данные о пользователе в формате JSON
     * @return Уникальный идентификатор пользователя в формате JSON
     */
    @RequestMapping(value = "/user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserIdJson> createUser(@RequestBody User user) {
        String name = user.getName();
        String email = user.getEmail();
        if (name == null || name.isEmpty() || email == null || email.isEmpty()) {
            return new ResponseEntity(HttpStatus.PARTIAL_CONTENT);
        }

        try {
            User newUser = userService.createUser(user);
            return new ResponseEntity(new UserIdJson(newUser.getId()), HttpStatus.CREATED);
        } catch (ConstraintViolationException ex) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    /**
     * Возможные коды ответов:<br/>
     * 200 - OK<br/>
     * 404 - Not Found<br/>
     * 406 - Not Acceptable<br/>
     *
     * @param id Уникальный идентификатор пользователя
     * @return персональные данные пользователя в JSON формате
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(user, HttpStatus.OK);
    }

    /**
     * Возможные коды ответов:<br/>
     * 200 - OK<br/>
     * 404 - Not Found<br/>
     * 406 - Not Acceptable<br/>
     *
     * @param id Уникальный идентификатор пользователя
     * @param newStatus Новый статус пользователя. Возможные варианты: online,
     * offline
     * @return
     */
    @RequestMapping(value = "/user/{id}/{status}", method = RequestMethod.PUT)
    public ResponseEntity<UserStatusJson> changeUserStatus(@PathVariable("id") Long id, @PathVariable("status") String newStatus) {
        newStatus = newStatus.toLowerCase();
        if (!newStatus.equals("online") && !newStatus.equals("offline")) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }

        String oldStatus = userService.changeStatus(id, newStatus);
        if (oldStatus == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(new UserStatusJson(id, oldStatus, newStatus), HttpStatus.OK);
    }

    /**
     * Возможные коды ответов:<br/>
     * 200 - OK<br/>
     *
     * @param statsReq объект запроса, содержит статус и уникальный ID
     * (timestamp) запроса в JSON формате
     * @return список пользователей со статусами и URI картинки, а также
     * уникальный ID (timestamp) запроса в JSON формате
     */
    @RequestMapping(value = "/stats", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public StatsJson getStats(@RequestBody StatsReqJson statsReq) {
        String status = null;
        Long timestamp = null;
        if (statsReq != null) {
            status = statsReq.getStatus();
            timestamp = statsReq.getTimestamp();
        }

        List<User> users = userService.getByStatus(status);
        return new StatsJson(users, timestamp != null ? timestamp : System.currentTimeMillis());
    }

}
