package kz.nur4i.technodom.json;

/**
 *
 * @author Aibek
 */
public class UserIdJson {

    private long id;

    public UserIdJson(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
