package kz.nur4i.technodom.service;

import java.util.List;
import kz.nur4i.technodom.dao.UserDao;
import kz.nur4i.technodom.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Aibek
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User createUser(User user) {
        return userDao.create(user);
    }

    @Override
    public User getUser(Long id) {
        return userDao.get(id);
    }

    @Override
    public String changeStatus(Long id, String newStatus) {
        if (id == null) {
            return null;
        }

        User user = getUser(id);
        if (user == null) {
            return null;
        }

        String oldStatus = user.getStatus();
        user.setStatus(newStatus);
        userDao.update(user);

        return oldStatus;
    }

    @Override
    public List<User> getByStatus(String status) {
        return userDao.getByStatus(status);
    }

}
